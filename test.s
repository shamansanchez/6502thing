.include "addr.inc"

.export _init

.code
_init:
  ldx #$ff
  txs
  ldx #0

  jsr clear_screen

  ldy #0
print_loop:
  tya
  sta video::BASE, Y
  iny
  jmp print_loop



clear_screen:
  pha
  phy

start_clear:
  ldy #0
  lda #$20

loop_clear:
  sta video::BASE, Y
  sta video::BASE+$100, Y
  iny
  beq done_clear
  jmp loop_clear

done_clear:
  ply
  pla
  rts
