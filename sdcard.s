.include "addr.inc"
.include "macros.inc"
.autoimport

.scope sdcmds
CMD0:
  .byte $40,$00,$00,$00,$00,$95
CMD8:
  .byte $48,$00,$00,$01,$AA,$87
CMD55:
  .byte $77,$00,$00,$00,$00,$01
CMD58:
  .byte $7A,$00,$00,$00,$00,$01
CMD41:
  .byte $69,$40,$00,$00,$00,$01
CMD16:
  .byte $50,$00,$00,$02,$00,$01

CMD17:
  .byte $51,$00,$00,$00,$00,$01
.endscope

.export sd_init
.proc sd_init
  pha
  phx

  ldx #80
  lda #$FF

loop:
  jsr spi_byte_sd
  lda #$FF
  dex
  bne loop

;  lda #$FF
;  jsr spi_byte_sd

  stz spi::SD
  do_sd_cmd sdcmds::CMD0
  jsr sd_readwait
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  do_sd_cmd sdcmds::CMD8
  jsr sd_readwait
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #spi::SELECT
  sta spi::SD

retry:
  stz spi::SD
  do_sd_cmd sdcmds::CMD55
  jsr sd_readwait
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  do_sd_cmd sdcmds::CMD41
  jsr sd_readwait
  pha
  lda #spi::SELECT
  sta spi::SD

  pla
  bne retry

  stz spi::SD
  do_sd_cmd sdcmds::CMD58
  jsr sd_readwait
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #$FF
  jsr spi_byte_sd
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  do_sd_cmd sdcmds::CMD16
  jsr sd_readwait
  lda #spi::SELECT
  sta spi::SD

  plx
  pla
  rts
.endproc

; =============================================================
.export sd_read_block
.proc sd_read_block
  phx
  phy

  stz spi::SD
  do_sd_cmd sdcmds::CMD17
  jsr sd_readwait ; response
  jsr sd_readwait ; data packet
  lda #spi::SELECT
  sta spi::SD

  lda #$00
  sta TMP_LOW
  lda #$7D
  sta TMP_HIGH


  stz spi::SD

  ldx #$01
  ldy #$FF

read:
  lda #$FF
  jsr spi_byte_sd

  sta (TMP_LOW)
  inc TMP_LOW
  bne next
  inc TMP_HIGH

next:
  tya
  bne decy
  txa
  beq done
  dex
decy:
  dey
  jmp read

done:
  lda #spi::SELECT
  sta spi::SD

  stz spi::SD
  jsr sd_readwait ; checksum
  jsr sd_readwait ; checksum
  lda #spi::SELECT
  sta spi::SD

  ply
  plx
  rts
.endproc

; =============================================================
.proc sd_readwait
  lda #$FF
  jsr spi_byte_sd

  cmp #$FF
  beq sd_readwait

  rts
.endproc

; =============================================================
.proc sd_cmd
  phy
  phx

  ldx #6
  ldy #0

loop:
  lda (sd::CMD_ADDR_LOW),Y
  jsr spi_byte_sd
  iny
  dex
  beq done
  jmp loop

done:
  plx
  ply
  rts
.endproc
