.macro push
  dex
  dex
.endmacro

.macro pop
  inx
  inx
.endmacro

.macro loadaddr addr
  lda #<addr
  sta TMP_LOW
  lda #>addr
  sta TMP_HIGH
.endmacro

.macro pushaddr addr
  push
  lda #<addr
  sta 0,X
  lda #>addr
  sta 1,X
.endmacro


.macro pushbyte byte
  push
  lda byte
  sta 0,X
  stz 1,X
.endmacro

.macro do_sd_cmd addr
  lda #<addr
  sta sd::CMD_ADDR_LOW
  lda #>addr
  sta sd::CMD_ADDR_HIGH
  jsr sd_cmd
.endmacro


.macro store_vram
  sta video2::VRAM
  nop
  nop
  nop
.endmacro

.macro load_vram
  lda video2::VRAM
  nop
  nop
  nop
.endmacro

.macro store_register
  sta video2::REG
  nop
  nop
  nop
.endmacro

.macro load_register
  lda video2::REG
  nop
  nop
  nop
.endmacro
