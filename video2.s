.include "addr.inc"
.include "macros.inc"
.autoimport

.code

; ================================
.export video_init_text
.proc video_init_text
  lda #%00000000
  sta video2::REG
  lda #$80
  sta video2::REG

  lda #%11110000
  sta video2::REG
  lda #$81
  sta video2::REG

  lda #$02
  sta video2::REG
  lda #$82
  sta video2::REG

  lda #$00
  sta video2::REG
  lda #$84
  sta video2::REG

  lda #$F6
  sta video2::SCREEN_COLOR

  jsr video_set_color

  lda #30
  sta video2::CURSOR_BLINK
  stz video2::CURSOR_SHOW

  rts
.endproc

; ================================
.export video_set_color
.proc video_set_color
  lda video2::SCREEN_COLOR
  sta video2::REG

  lda #$87
  sta video2::REG

  rts
.endproc

; ================================
.export video_init_graphics2
.proc video_init_graphics2
  lda #$02
  sta video2::REG
  lda #$80
  sta video2::REG

  lda #$C2
  sta video2::REG
  lda #$81
  sta video2::REG

  lda #$0E
  sta video2::REG
  lda #$82
  sta video2::REG

  lda #$FF
  sta video2::REG
  lda #$83
  sta video2::REG

  lda #$03
  sta video2::REG
  lda #$84
  sta video2::REG

  lda #$76
  sta video2::REG
  lda #$85
  sta video2::REG

  lda #$03
  sta video2::REG
  lda #$86
  sta video2::REG

  lda #$F6
  sta video2::REG
  lda #$87
  sta video2::REG

  rts
.endproc

; ================================
.export video_load_font
.proc video_load_font
  lda #$00
  sta video2::REG
  lda #$40
  sta video2::REG

  lda #<font_data
  sta video2::CURSOR_LOW
  lda #>font_data
  sta video2::CURSOR_HIGH

  stz TMP_LOW
  lda #$08
  sta TMP_HIGH

loop:
  lda (video2::CURSOR_LOW)
  sta video2::VRAM

  lda TMP_LOW
  bne decl
  lda TMP_HIGH
  beq done
  dec TMP_HIGH
decl:
  dec TMP_LOW

  inc video2::CURSOR_LOW
  bne loop
  inc video2::CURSOR_HIGH

  jmp loop

done:
  rts
.endproc

; ================================
.export print_str
.proc print_str

print_loop:  ; string_addr --
  lda (TMP_LOW)
  beq print_done

  jsr print_char

  inc TMP_LOW
  bne print_loop

  inc TMP_HIGH
  jmp print_loop

print_done:
  rts
.endproc

; ================================
.export print_char
.proc print_char
  pha
  phx
  phy

  cmp #$0D
  beq cr
  cmp #$0A
  beq lf
  cmp #$08
  beq backspace

  jsr print_char_raw
  jmp done

cr:
  ldx #0
  ldy video2::POS_Y
  jsr set_cursor
  jmp done

lf:
  jsr newline
  jmp done

backspace:
  dec video2::POS_X
  bpl move
  lda #39
  sta video2::POS_X
  dec video2::POS_Y
move:
  ldx video2::POS_X
  ldy video2::POS_Y
  jsr set_cursor

  lda #$00
  jsr print_char_raw

  dec video2::POS_X
  bpl move2
  lda #39
  sta video2::POS_X
  dec video2::POS_Y
move2:
  ldx video2::POS_X
  ldy video2::POS_Y
  jsr set_cursor

done:
  ply
  plx
  pla
  rts

.endproc

;=====================================
.proc print_char_raw

  sta (video2::CURSOR_LOW)
  inc video2::CURSOR_LOW
  bne next
  inc video2::CURSOR_HIGH

next:
  inc video2::POS_X
  ldx video2::POS_X
  cpx #40
  bne done

  stz video2::POS_X
  inc video2::POS_Y

  ldy video2::POS_Y
  cpy #24
  bne done
  jsr scroll_line

done:
  rts
.endproc

; ================================
.export scroll_line
.proc scroll_line
  pha
  phx
  phy

  lda #1
  sta video2::SCROLL_LOW1

next:
  ldx #0
  ldy video2::SCROLL_LOW1
  jsr set_cursor

  ldy #40
readline:
  lda (video2::CURSOR_LOW),Y
  sta video2::SCROLL_BUF,Y
  dey
  bpl readline

  ldx #0
  ldy video2::SCROLL_LOW1
  dey
  jsr set_cursor

  ldy #40
writeline:
  lda video2::SCROLL_BUF,Y
  sta (video2::CURSOR_LOW),Y
  dey
  bpl writeline

  inc video2::SCROLL_LOW1
  lda video2::SCROLL_LOW1
  cmp #24
  beq clear

  jmp next

clear:
  ldx #0
  ldy #23
  jsr set_cursor

  ldy #40
clearline:
  lda #0
  sta (video2::CURSOR_LOW),Y
  dey
  bpl clearline

done:
  ply
  plx
  pla
  rts
.endproc

; ================================
.export set_cursor
.proc set_cursor
  pha
  phx
  phy

  stx video2::POS_X
  sty video2::POS_Y

  lda #40
  sta math::FACTOR1
  sty math::FACTOR2
  jsr mult

  clc
  txa
  adc math::FACTOR1
  sta video2::CURSOR_LOW
  lda #$70
  adc math::FACTOR2
  sta video2::CURSOR_HIGH

  ply
  plx
  pla
  rts
.endproc

; ================================
.export newline
.proc newline
  pha
  phx
  phy

  lda video2::POS_Y
  clc
  adc #1
  cmp #24
  bne no
  jsr scroll_line
  jmp done
no:
  tay
  ldx #0

  jsr set_cursor

done:
  ply
  plx
  pla
  rts
.endproc

; ================================
.export clear_screen
.proc clear_screen
  phx
  phy

  ldx #0
loop1:
  stz video2::SCREEN1,X
  inx
  bne loop1

  ldx #0
loop2:
  stz video2::SCREEN2,X
  inx
  bne loop2

  ldx #0
loop3:
  stz video2::SCREEN3,X
  inx
  bne loop3

  ldx #0
loop4:
  stz video2::SCREEN4,X
  inx
  cpx #$C0
  bne loop4

  ldx #0
  ldy #0
  jsr set_cursor

  ply
  plx
  rts
.endproc

; ==============================
.export print_byte
.proc print_byte  ;  str_bytes --
  pha
  phx

  jsr str_byte

  jsr print_char
  txa
  jsr print_char

  plx
  pla
  rts
.endproc

; ================================
.export load_lapras_pattern
.proc load_lapras_pattern
  lda #$00
  sta video2::REG
  lda #$40
  sta video2::REG

  lda #<lapras_pattern
  sta video2::CURSOR_LOW
  lda #>lapras_pattern
  sta video2::CURSOR_HIGH

  stz TMP_LOW
  lda #$18
  sta TMP_HIGH

loop:
  lda (video2::CURSOR_LOW)
  sta video2::VRAM
  nop
  nop
  nop
  nop

  lda TMP_LOW
  bne decl
  lda TMP_HIGH
  beq done
  dec TMP_HIGH
decl:
  dec TMP_LOW

  inc video2::CURSOR_LOW
  bne loop
  inc video2::CURSOR_HIGH

  jmp loop

done:
  rts
.endproc

; ================================
.export load_lapras_color
.proc load_lapras_color
  lda #$00
  sta video2::REG
  lda #$60
  sta video2::REG

  lda #<lapras_color
  sta video2::CURSOR_LOW
  lda #>lapras_color
  sta video2::CURSOR_HIGH

  stz TMP_LOW
  lda #$18
  sta TMP_HIGH

loop:
  lda (video2::CURSOR_LOW)
  sta video2::VRAM
  nop
  nop
  nop
  nop

  lda TMP_LOW
  bne decl
  lda TMP_HIGH
  beq done
  dec TMP_HIGH
decl:
  dec TMP_LOW

  inc video2::CURSOR_LOW
  bne loop
  inc video2::CURSOR_HIGH

  jmp loop

done:
  rts
.endproc
