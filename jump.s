.segment "JUMP"
.autoimport

.export heck_kbd_get_char
heck_kbd_get_char:
  jmp kbd_get_char

.export heck_print_char
heck_print_char:
  jmp print_char

.export heck_rx_get_char
heck_rx_get_char:
  jmp rx_get_char

.export heck_tx_put_char
heck_tx_put_char:
  jmp tx_put_char

.export heck_video_set_color
heck_video_set_color:
  jmp video_set_color
