.include "../addr.inc"
.include "../jump.table"
.code

looop:
  lda #$cf
  sta (video::CURSOR_LOW)

  jsr heck_kbd_get_char
  bcc get
  jsr heck_tx_put_char

get:
  jsr heck_rx_get_char
  bcc looop
  jsr heck_print_char

  jmp looop
