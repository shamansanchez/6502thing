.include "addr.inc"
.include "macros.inc"
.autoimport

.export _irq
_irq:
  pha
  phx
  phy

  tsx
  lda $0104,X  ; check for BRK
  and #$10
  bne break

  bit via2::IFR
  bmi keyboard

  lda video2::REG
  bmi timer

  jmp serial

break:
  jmp _monitor

keyboard:
  lda via2::PORTA
  jsr kbd_parse_char
  jmp irq_done

timer:
  lda #1
  sta via::PORTB

  lda #$00
  sta video2::REG
  lda #$48
  sta video2::REG

  dec video2::CURSOR_BLINK
  bne next
  lda #30
  sta video2::CURSOR_BLINK
  lda video2::CURSOR_SHOW
  eor #$80
  sta video2::CURSOR_SHOW


next:
  ldx #0
loop1:
  lda video2::SCREEN1,X
  sta video2::VRAM
  inx
  bne loop1

  bit via2::IFR ; bail early if there's a keyboard interrupt
  bmi keyboard  ; just draw on the next frame
  ;jsr serial_byte

  ldx #0
loop2:
  lda video2::SCREEN2,X
  sta video2::VRAM
  inx
  bne loop2

  bit via2::IFR
  bmi keyboard
  ;jsr serial_byte

  ldx #0
loop3:
  lda video2::SCREEN3,X
  sta video2::VRAM
  inx
  bne loop3

  bit via2::IFR
  bmi keyboard
  ;jsr serial_byte

  ldx #0
loop4:
  lda video2::SCREEN4,X
  sta video2::VRAM
  inx
  cpx #$C0
  bne loop4

  bit via2::IFR
  bmi keyboard

  bit video2::CURSOR_SHOW
  bpl serial
  bvs serial

  lda video2::CURSOR_LOW
  sta video2::REG
  lda video2::CURSOR_HIGH
  sec
  sbc #$28
  sta video2::REG
  lda #$DB
  sta video2::VRAM

serial:
  stz via::PORTB
  jsr serial_byte

irq_done:
  ply
  plx
  pla
  rti



.export _nmi
_nmi:
  nop
  rti
