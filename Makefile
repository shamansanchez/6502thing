OBJ = init.o irq.o monitor.o term.o video2.o jump.o vectors.o keyboard.o math.o strings.o spi.o serial.o sdcard.o io.o basic.o text.o font.o

jason.rom labels.out out.map: $(OBJ) mem.cfg
	ld65 -C mem.cfg -vm -Ln labels.out -m out.map $(OBJ) none.lib -o $@
	hexdump -C $@

jump.table: labels.out
	cat labels.out | grep .heck | awk '{sub(/^00/, "$$", $$2); sub(/^\./, "", $$3); print $$3 "=" $$2}' > jump.table

%.o: %.s addr.inc macros.inc
	ca65 --feature labels_without_colons --cpu 65c02 -o $@ $<

# bleh.s: bleh.c
# 	cc65 bleh.c

.PHONY: write sim clean
write: jason.rom
	sudo minipro -p AT28C256 -w jason.rom

sim: jason.rom
	sudo python EPROM_EMU_NG_2.0rc9.py -mem 28256 jason.rom /dev/ttyUSB0

clean:
	rm *.o jump.table labels.out out.map jason.rom
