#!/usr/bin/python3

import argparse
import sys
import time

import pygame
from py65 import monitor
from py65.devices.mpu65c02 import MPU as CMOS65C02
from py65.memory import ObservableMemory


class Heck(monitor.Monitor):
    def __init__(self):
        super().__init__(mpu_type=CMOS65C02, argv="", putc_addr=None, getc_addr=None)

        self.font = pygame.font.Font("HotCoCo.ttf", 24)
        self.font_width, self.font_height = self.font.size("A")
        self.disp = pygame.display.set_mode(
            (32 * self.font_width, 16 * self.font_height)
        )
        mem = ObservableMemory(subject=self.memory)

        self._mpu.memory = mem
        self.onecmd("load jason.rom 8000")
        self._install_io()

    def _install_io(self):
        def thing(addr, val):
            # print(hex(self._mpu.pc), ":r", hex(addr), "->", hex(val))
            pass

        def thing2(addr, val):
            # print(hex(self._mpu.pc), ":w", hex(addr), "<-", hex(val))

            char = addr - 0x8800
            val += 0xE000

            # print(chr(val), hex(val), char)

            X = char % 32
            Y = char // 32

            twidth, theight = self.font.size("o")
            X *= twidth
            Y *= theight

            try:
                if val <= 0xE0FF:
                    color = (176, 28, 1)
                if val <= 0xE0EF:
                    color = (152, 1, 230)
                if val <= 0xE0DF:
                    color = (14, 180, 97)
                if val <= 0xE0CF:
                    color = (126, 126, 119)
                if val <= 0xE0BF:
                    color = (118, 0, 3)
                if val <= 0xE0AF:
                    color = (20, 0, 215)
                if val <= 0xE09F:
                    color = (143, 196, 7)
                if val <= 0xE08F:
                    color = (18, 252, 7)

                text = self.font.render(chr(val), True, color)
                rect = text.get_rect()

                rect.center = (X + (twidth // 2), Y + (theight // 2))

                if val > 0xE07F:
                    self.disp.fill((0, 6, 0), rect)
                else:
                    self.disp.fill((5, 116, 1), rect)

                self.disp.blit(text, rect)
                pygame.display.update()
                time.sleep(0.0)
                if val != 0xE020:
                    # time.sleep(0.0001)
                    pass
            except pygame.error:
                pass
            except ValueError:
                pass

        self._mpu.memory.subscribe_to_write(list(range(0x8800, 0x8A00)), thing2)


pygame.init()

heck = Heck()
heck._mpu.pc = 0xA000
heck._run([0x00])
