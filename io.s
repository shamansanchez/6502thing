.include "addr.inc"
.autoimport

;====================================================================
.export rx_get_char
.proc rx_get_char
  phx
loop:
  sei
  lda io::RX_READ
  cmp io::RX_WRITE
  cli
  beq none

  ldx io::RX_READ
  lda io::RX_BUFF, X
  inc io::RX_READ
  sec
  jmp some

none:
  clc
some:
  plx
  rts
.endproc

;====================================================================
.export rx_get_char_blocking
.proc rx_get_char_blocking
  jsr rx_get_char
  bcc rx_get_char_blocking
  rts
.endproc

;====================================================================
.export rx_put_char
.proc rx_put_char
  phx

  ldx io::RX_WRITE
  sta io::RX_BUFF, X

  inc io::RX_WRITE

  plx
  rts
.endproc

;====================================================================
.export tx_put_char
.proc tx_put_char
  phx

  ldx io::TX_WRITE
  sta io::TX_BUFF, X

  inc io::TX_WRITE

  sei
  jsr serial_byte
  cli

  plx
  rts
.endproc

; ==============================
.export tx_byte
.proc tx_byte
  pha
  phx

  jsr str_byte

  jsr tx_put_char
  txa
  jsr tx_put_char

  plx
  pla
  rts
.endproc


;====================================================================
.export rx_buff_used
.proc rx_buff_used
  phx

  lda io::RX_WRITE
  sec
  sbc io::RX_READ

  plx
  rts
.endproc


;====================================================================
.export xmodem_recv
.proc xmodem_recv
  phx
  phy

  ; store at $0700
  stz io::COPY_COUNTER_LOW
  lda #$07
  sta io::COPY_COUNTER_HIGH

  lda #1
  sta io::XMODEM_PACKETNUM

initnak:
  lda #$15
  jsr tx_put_char

packet:
  stz io::XMODEM_CHECKSUM
  jsr rx_get_char_blocking  ; SOH

  cmp #$04
  beq done
  cmp #$01
  bne initnak

  jsr rx_get_char_blocking ; packet num
  jsr rx_get_char_blocking  ; packet num

  ldx #$80
  ldy #$00

recv:
  jsr rx_get_char_blocking

  sta strbuff::BASE,Y
  iny

  clc
  adc io::XMODEM_CHECKSUM
  sta io::XMODEM_CHECKSUM

  dex
  bne recv

  jsr rx_get_char_blocking ; checksum
  cmp io::XMODEM_CHECKSUM
  beq ack

nak:
  lda #'X'
  jsr print_char
  lda #$15
  jsr tx_put_char
  jmp packet
ack:
  lda #'.'
  jsr print_char
  jsr copy_packet
  lda #$06
  jsr tx_put_char
  jmp packet

done:
  lda #$06
  jsr tx_put_char

  ply
  plx
  rts
.endproc

;====================================================================
.proc copy_packet
  pha
  phy

  ldy #0

loop:
  lda strbuff::BASE,Y
  sta (io::COPY_COUNTER_LOW)
  inc io::COPY_COUNTER_LOW
  bne cont
  inc io::COPY_COUNTER_HIGH
cont:
  iny
  bmi done
  jmp loop

done:
  ply
  pla
  rts
.endproc
