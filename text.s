.export message, message2, message3
message: .byte "WHAT THE HECK",$0a,$00
message2: .byte "WOW melissa",$0a,$00
message3: .byte "Jason was not here.",$0a,$00

.export ringo_line1, ringo_line2
ringo_line1: .byte "it's ya boi",$0a,$00
ringo_line2: .byte "ringo beep beep beep beep beep",$0a,$00

.export long_message
long_message:
.byte "this used to be long",$0a,$00

.export test_prompt
test_prompt:
.byte "jason",$00

.export type_test
type_test:
.byte "jasonnnnnnnn",$0a
.byte "jason",$0a
.byte "jasona",$0a
.byte "jason",$0a
.byte $00

.export wow_melissa
wow_melissa:
.byte $e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$0a
.byte $e0,$e0,$e0,$e0,$e7,$ef,$ef,$eb,$e0,$e0,$e0,$e0,$0a
.byte $e0,$e0,$e0,$e7,$ef,$ef,$ef,$ef,$eb,$e0,$e0,$e0,$0a
.byte $e0,$e9,$e0,$ef,$cf,$ef,$cf,$ef,$ef,$e0,$e6,$e0," WOW MELISSA",$0a
.byte $e0,$e0,$e9,$ef,$ef,$ef,$ef,$ef,$ef,$e6,$e0,$e0,$0a
.byte $e0,$e0,$e0,$ef,$e1,$e2,$ef,$ef,$ef,$e0,$e0,$e0,$0a
.byte $e0,$e0,$e0,$ed,$ee,$ed,$ee,$ed,$ee,$e0,$e0,$e0,$0a
.byte $e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$0a

.byte $00


.export wow_jason
wow_jason:
.byte $f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$0a
.byte $f0,$f0,$f0,$f0,$f7,$ff,$ff,$fb,$f0,$f0,$f0,$f0,$0a
.byte $f0,$f0,$f0,$f7,$ff,$ff,$ff,$ff,$fb,$f0,$f0,$f0,$0a
.byte $f0,$f9,$f0,$ff,$cf,$ff,$cf,$ff,$ff,$f0,$f6,$f0," WOW JASON",$0a
.byte $f0,$f0,$f9,$ff,$ff,$ff,$ff,$ff,$ff,$f6,$f0,$f0,$0a
.byte $f0,$f0,$f0,$ff,$f1,$f2,$ff,$ff,$ff,$f0,$f0,$f0,$0a
.byte $f0,$f0,$f0,$fd,$fe,$fd,$fe,$fd,$fe,$f0,$f0,$f0,$0a
.byte $f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$0a

.byte $00

.export wow_jackie
wow_jackie:
.byte $d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$0a
.byte $d0,$d0,$d0,$d0,$d7,$df,$df,$db,$d0,$d0,$d0,$d0,$0a
.byte $d0,$d0,$d0,$d7,$df,$df,$df,$df,$db,$d0,$d0,$d0,$0a
.byte $d0,$d9,$d0,$df,$cf,$df,$cf,$df,$df,$d0,$d6,$d0," WOW JACKIE",$0a
.byte $d0,$d0,$d9,$df,$df,$df,$df,$df,$df,$d6,$d0,$d0,$0a
.byte $d0,$d0,$d0,$df,$d1,$d2,$df,$df,$df,$d0,$d0,$d0,$0a
.byte $d0,$d0,$d0,$dd,$de,$dd,$de,$dd,$de,$d0,$d0,$d0,$0a
.byte $d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0,$0a

.byte $00

.export xmodem_load_str
xmodem_load_str:
.asciiz "XMODEM load... :"

.export heck_logo
heck_logo:
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$0a,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$0a,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$0a,$20,$db,$20
.byte $20,$20,$20,$db,$20,$db,$db,$db,$db,$db
.byte $db,$20,$20,$b0,$db,$db,$db,$b1,$20,$db
.byte $20,$20,$20,$b1,$b2,$0a,$20,$db,$20,$20
.byte $20,$20,$db,$20,$db,$20,$20,$20,$20,$20
.byte $20,$b0,$db,$b1,$20,$b0,$db,$20,$db,$20
.byte $20,$b1,$db,$20,$0a,$20,$db,$20,$20,$20
.byte $20,$db,$20,$db,$20,$20,$20,$20,$20,$20
.byte $db,$b1,$20,$20,$20,$20,$20,$db,$20,$b1
.byte $db,$20,$20,$0a,$20,$db,$20,$20,$20,$20
.byte $db,$20,$db,$20,$20,$20,$20,$20,$20,$db
.byte $20,$20,$20,$20,$20,$20,$db,$b1,$db,$20
.byte $20,$20,$0a,$20,$db,$db,$db,$db,$db,$db
.byte $20,$db,$db,$db,$db,$db,$db,$20,$db,$20
.byte $20,$20,$20,$20,$20,$db,$db,$db,$b0,$20
.byte $20,$0a,$20,$db,$20,$20,$20,$20,$db,$20
.byte $db,$20,$20,$20,$20,$20,$20,$db,$20,$20
.byte $20,$20,$20,$20,$db,$20,$b1,$db,$20,$20
.byte $0a,$20,$db,$20,$20,$20,$20,$db,$20,$db
.byte $20,$20,$20,$20,$20,$20,$db,$b1,$20,$20
.byte $20,$20,$20,$db,$20,$20,$db,$b2,$20,$0a
.byte $20,$db,$20,$20,$20,$20,$db,$20,$db,$20
.byte $20,$20,$20,$20,$20,$b0,$db,$b1,$20,$b0
.byte $b2,$20,$db,$20,$20,$20,$db,$b0,$0a,$20
.byte $db,$20,$20,$20,$20,$db,$20,$db,$db,$db
.byte $db,$db,$db,$20,$20,$b1,$db,$db,$db,$b1
.byte $20,$db,$20,$20,$20,$b1,$db,$0a,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$0a,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$0a,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$20,$20,$20,$20,$20,$20
.byte $20,$20,$20,$20,$0a

.byte "(T)erminal   (B)asic   (M)onitor",$0a
.byte "Load program over (X)MODEM",$0a
.byte "(J)ump to loaded program",$0a
.byte "? ", $00

.export lapras_pattern
lapras_pattern:
;.incbin "./gfx/LAPRAS.TIAP"

.export lapras_color
lapras_color:
;.incbin "./gfx/LAPRAS.TIAC"
