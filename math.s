.include "addr.inc"
.include "macros.inc"


.export mult
.proc mult
  phx

  lda #$0
  ldx #$8
  lsr math::FACTOR1
loop:
  bcc no
  clc
  adc math::FACTOR2
no:
  ror
  ror math::FACTOR1
  dex
  bne loop
  sta math::FACTOR2

  plx
  rts
.endproc
