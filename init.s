.include "addr.inc"
.include "macros.inc"
.autoimport

.export _init

.code
_init:
  ldx #$ff
  txs

  ldx #$00

  stz keyboard::FLAGS

  stz strbuff::CURSOR
  stz io::TX_READ
  stz io::TX_WRITE
  stz io::RX_READ
  stz io::RX_WRITE
  stz keyboard::READ
  stz keyboard::WRITE

  jsr init_serial

  lda #$FF
  sta via::DDRB

  jsr video_init_text
  jsr video_load_font

.export _reset
_reset:
  jsr clear_screen
  lda #30
  sta video2::CURSOR_BLINK
  stz video2::CURSOR_SHOW

  cli

  loadaddr heck_logo
  jsr print_str

loop:
  jsr rx_get_char
  jsr kbd_get_char
  bcc loop

  cmp #'t'
  beq term

  cmp #'x'
  beq xmodem

  cmp #'j'
  beq jmp700

  cmp #'b'
  beq basic

  cmp #'m'
  beq monitor

  jmp loop

term:
  jsr clear_screen
  jmp _term

xmodem:
  loadaddr xmodem_load_str
  jsr print_str
  jsr xmodem_recv

jmp700:
  jsr clear_screen
  lda #$40
  trb video2::CURSOR_SHOW
  jmp $0700

basic:
  jsr clear_screen
  lda #$40
  trb video2::CURSOR_SHOW
  jmp _basic

monitor:
  jsr clear_screen
  jmp _monitor
