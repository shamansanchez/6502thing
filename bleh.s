.include "addr.inc"
.export _init

.import message
.import message2
.import message3

.code
_init:
  ldx #$ff
  txs
  ldx #0

  stz video::CURSOR
  stz PRINT_LOW
  stz PRINT_HIGH

delay:
  nop
  nop
  nop
  inx
  beq start
  jmp delay

start:
  ldy #0
  lda #$20

loop:
  sta video::BASE, Y
  sta video::BASE+100, Y
  iny
  beq done
  jmp loop

done:
  lda #<message
  sta PRINT_LOW
  lda #>message
  sta PRINT_HIGH
  jsr print

  jsr newline

  lda #<message2
  sta PRINT_LOW
  lda #>message2
  sta PRINT_HIGH
  jsr print

  jsr newline
  jsr newline

  lda #<message3
  sta PRINT_LOW
  lda #>message3
  sta PRINT_HIGH
  jsr print

end:
  nop
  jmp end



print:
  phy
  phx
  pha
  ldy #0

print_loop:
  lda (PRINT_LOW), Y
  beq print_done

  ldx video::CURSOR
  sta video::BASE, X

  inc video::BASE
  iny
  jmp print_loop

print_done:
  pla
  plx
  ply
  rts


newline:
  pha
  phx

  lda #0
  ldx video::BASE

  cpx #$20
  bcc l7

  cpx #$40
  bcc l6

  cpx #$60
  bcc l5

  cpx #$80
  bcc l4

  cpx #$A0
  bcc l3

  cpx #$C0
  bcc l2

  cpx #$E0
  bcc l1


l1:
  adc #$20
l2:
  adc #$20
l3:
  adc #$20
l4:
  adc #$20
l5:
  adc #$20
l6:
  adc #$20
l7:
  adc #$20

  sta video::BASE

  plx
  pla
  rts
