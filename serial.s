.include "addr.inc"
.autoimport

;==============================================================
.export init_serial
.proc init_serial
  stz serial::RTSB

  lda #%00000000
  sta via2::DDRA    ; set port A to input

  lda #%00000001
  sta via2::PCR     ; interrupt on positive edge

  lda #%00000111
  sta via2::DDRB    ; configure port B for SPI
  sta via::DDRA    ; configure port A for SPI

  lda #%01000000
  sta via2::ACR     ; enable T1

  lda #%10000010   ;enable interrupts from CA1
  sta via2::IER

  lda #spi::SELECT
  sta spi::SERIAL
  sta spi::SD

  stz spi::SERIAL
  lda #%11001100
  jsr spi_byte
  lda #%00001001 ; 19.2k
  ;lda #%00000011 ; 14.4k
  ;lda #%00001010 ; 9600
  jsr spi_byte
  lda #spi::SELECT
  sta spi::SERIAL

  stz spi::SERIAL
  lda #%01000000
  jsr spi_byte
  lda #%00000000
  jsr spi_byte
  lda #spi::SELECT
  sta spi::SERIAL
.endproc

;==============================================================
.export serial_byte
.proc serial_byte
  phx

checkrts:
  jsr rx_buff_used
  cmp #$0F
  bcc enable
  cmp #$F0
  bcc check

  lda #2
  sta serial::RTSB
  jmp check

enable:
  stz serial::RTSB

check:
  lda io::TX_WRITE
  cmp io::TX_READ
  beq nodata
  lda #%10000010
  jmp write

nodata:
  lda #%10000110
write:
  eor serial::RTSB
  stz via2::PORTB

  jsr spi_byte
  sta serial::STATUS

  lda io::TX_WRITE
  cmp io::TX_READ
  beq read

  bit serial::STATUS
  bvc read

  ldx io::TX_READ
  lda io::TX_BUFF,X
  inc io::TX_READ
read:
  jsr spi_byte
  sta serial::BYTE

  lda #spi::SELECT
  sta via2::PORTB

  bit serial::STATUS
  bpl end

  lda serial::BYTE
  jsr rx_put_char

end:
  plx
  rts
.endproc
