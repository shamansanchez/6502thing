.include "addr.inc"
.include "macros.inc"
.autoimport

.code

;====================================================================
.export kbd_get_char
.proc kbd_get_char
  phx
loop:
  sei
  lda keyboard::READ
  cmp keyboard::WRITE
  cli
  beq none

  ldx keyboard::READ
  lda keyboard::BASE, X
  inc keyboard::READ
  sec
  jmp some

none:
  clc
some:
  plx
  rts
.endproc

;====================================================================
.export kbd_put_char
.proc kbd_put_char
  phx

  ldx keyboard::WRITE
  sta keyboard::BASE, X

  inc keyboard::WRITE

  plx
  rts
.endproc

;====================================================================
.export kbd_push_str
.proc kbd_push_str  ; addr --
  pha
  phx

loop:
  lda (0,X)
  beq done

  jsr kbd_put_char

  inc 0,X
  bne loop

  inc 1,X
  jmp loop

done:
  pop
  plx
  pla
  rts
.endproc

;====================================================================
.export kbd_parse_char
.proc kbd_parse_char
  pha
  phx
  phy

  cmp #$aa
  beq jmpdone
  cmp #$ff
  beq jmpdone
  cmp #$00
  beq jmpdone

  tay

  lda keyboard::FLAGS
  and #keyboard::RELEASE
  beq read

  lda #keyboard::RELEASE
  trb keyboard::FLAGS

  lda #keyboard::EXTENDED
  trb keyboard::FLAGS

  tya
  cmp #$05
  beq reset

  cmp #$12
  beq shift_up
  cmp #$59
  beq shift_up

  cmp #$14
  beq ctrl_up

jmpdone:
  jmp done

reset:
  jmp _reset

shift_up:
  lda #keyboard::SHIFT
  trb keyboard::FLAGS
  jmp done

ctrl_up:
  lda #keyboard::CTRL
  trb keyboard::FLAGS
  jmp done

read:
  tya

  cmp #$f0
  beq release
  cmp #$12
  beq shift
  cmp #$59
  beq shift

  cmp #$14
  beq ctrl

  cmp #$E0
  beq extend

  tax

  lda keyboard::FLAGS
  and #keyboard::SHIFT
  bne shifted

  lda keyboard::FLAGS
  and #keyboard::CTRL
  bne ctrled

  lda keyboard::FLAGS
  and #keyboard::EXTENDED
  bne extended

  lda keymap, X
  jsr kbd_put_char
  jmp done

shifted:
  lda keymap_shifted, X
  jsr kbd_put_char
  jmp done

ctrled:
  lda keymap_ctrled, X
  jsr kbd_put_char
  jmp done

extended:
  lda keymap_extended, X
  jsr kbd_put_char

  lda #keyboard::EXTENDED
  trb keyboard::FLAGS
  jmp done

shift:
  lda keyboard::FLAGS
  ora #keyboard::SHIFT
  sta keyboard::FLAGS
  jmp done

ctrl:
  lda keyboard::FLAGS
  ora #keyboard::CTRL
  sta keyboard::FLAGS
  jmp done

extend:
  lda keyboard::FLAGS
  ora #keyboard::EXTENDED
  sta keyboard::FLAGS
  jmp done

release:
  lda keyboard::FLAGS
  ora #keyboard::RELEASE
  sta keyboard::FLAGS

done:
  ply
  plx
  pla
  rts
.endproc

keymap:
  .byte "????????????? `?" ; 00-0F
  .byte "?????q1???zsaw2?" ; 10-1F
  .byte "?cxde43?? vftr5?" ; 20-2F
  .byte "?nbhgy6???mju78?" ; 30-3F
  .byte "?,kio09??./l;p-?" ; 40-4F
  .byte "??'?[=????",$0d,"]?\??" ; 50-5F
  .byte "??????",$08,"??1?47???" ; 60-6F
  .byte "0.2568",$1b,"??+3-*9??" ; 70-7F
  .byte "????????????????" ; 80-8F
  .byte "????????????????" ; 90-9F
  .byte "????????????????" ; A0-AF
  .byte "????????????????" ; B0-BF
  .byte "????????????????" ; C0-CF
  .byte "????????????????" ; D0-DF
  .byte "????????????????" ; E0-EF
  .byte "????????????????" ; F0-FF

keymap_shifted:
  .byte "????????????? ~?" ; 00-0F
  .byte "?????Q!???ZSAW@?" ; 10-1F
  .byte "?CXDE$#?? VFTR%?" ; 20-2F
  .byte "?NBHGY^???MJU&*?" ; 30-3F
  .byte "?<KIO)(??>?L:P_?" ; 40-4F
  .byte "??",$22,"?{+?????}?|??" ; 50-5F
  .byte "??????",$08,"??1?47???" ; 60-6F
  .byte "0.2568???+3-*9??" ; 70-7F
  .byte "????????????????" ; 80-8F
  .byte "????????????????" ; 90-9F
  .byte "????????????????" ; A0-AF
  .byte "????????????????" ; B0-BF
  .byte "????????????????" ; C0-CF
  .byte "????????????????" ; D0-DF
  .byte "????????????????" ; E0-EF
  .byte "????????????????" ; F0-FF

keymap_ctrled:
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 00-0F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 10-1F
  .byte $01,$03,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 20-2F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 30-3F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 40-4F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 50-5F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$1B,$01,$01,$01,$01 ; 60-6F
  .byte $01,$01,$19,$01,$1A,$18,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 70-7F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 80-8F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 90-9F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; A0-AF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; B0-BF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; C0-CF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; D0-DF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; E0-EF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; F0-FF

keymap_extended:
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 00-0F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 10-1F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 20-2F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 30-3F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 40-4F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 50-5F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$1B,$01,$01,$01,$01 ; 60-6F
  .byte $01,$01,$19,$01,$1A,$18,$01,$01,$01,$01,$1F,$01,$01,$1E,$01,$01 ; 70-7F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 80-8F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; 90-9F
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; A0-AF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; B0-BF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; C0-CF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; D0-DF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; E0-EF
  .byte $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01 ; F0-FF
