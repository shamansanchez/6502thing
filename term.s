.include "addr.inc"
.include "macros.inc"
.autoimport
.code

message:
  .asciiz "HECKterm"

.export _term
.proc _term
  loadaddr message
  jsr print_str
  jsr newline

  lda #$40
  trb video2::CURSOR_SHOW

loop:
  jsr kbd_get_char
  bcc get
  jsr tx_put_char

get:
  jsr rx_get_char
  bcc loop

  jsr print_char
  jmp loop
.endproc
