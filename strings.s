.include "addr.inc"
.include "macros.inc"

.autoimport

; =============================
.export str_eq ; (str1 str2 -- eq)
.proc str_eq
  pha
  phy

  lda (0,X)
  cmp (2,X)
  bne neq
loop:
  inc 0,X
  inc 2,X

  lda (0,X)
  beq null

  cmp (2,X)
  beq loop

  jmp neq

null:
  lda (2,X)
  beq eq

neq:
  pop
  stz 0,X
  stz 1,X
  jmp end

eq:
  pop
  lda #$01
  sta 0,X
  stz 1,X

end:
  ply
  pla
  rts
.endproc

; ===================================================================
.export str_byte
.proc str_byte
  pha
  and #%00001111
  jsr nybble
  tax

  pla
  lsr
  lsr
  lsr
  lsr

nybble:
  cmp #10
  bcc done
  adc #6
done:
  adc #$30
  rts
.endproc

; ===================================================================
.export str_int
.proc str_int
  stz math::RESULT_LOW
  stz math::RESULT_HIGH

loop:
  lda (TMP_LOW)
  jsr char_digit
  bcc done

  lda math::RESULT_LOW
  sta math::FACTOR1
  lda #10
  sta math::FACTOR2
  jsr mult

  lda math::FACTOR1
  sta math::RESULT_LOW

  lda (TMP_LOW)
  sec
  sbc #$30

  clc
  adc math::RESULT_LOW
  sta math::RESULT_LOW

  inc TMP_LOW
  bne loop
  inc TMP_HIGH
  jmp loop

done:
  rts
.endproc

; ===================================================================
.export char_digit
.proc char_digit
  clc

  cmp #$3A
  bcs clear

  cmp #$30
  bcc done

  jmp set

clear:
  clc
  jmp done

set:
  sec
done:
  rts

.endproc
