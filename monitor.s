.include "addr.inc"
.include "macros.inc"
.autoimport

.code

;=======================================================
.export _monitor
.proc _monitor
  cli
  stz keyboard::FLAGS

  stz monitor::CURR_ADDR_LOW
  stz monitor::CURR_ADDR_HIGH

  lda #$40
  tsb video2::CURSOR_SHOW

disploop:
  ldx #0
  ldy #1
  jsr set_cursor

  lda monitor::CURR_ADDR_HIGH
  sta monitor::CURR_LINE_HIGH
  lda monitor::CURR_ADDR_LOW
  sta monitor::CURR_LINE_LOW

  ldx #20
screenloop:
  lda monitor::CURR_LINE_HIGH
  jsr print_byte
  lda monitor::CURR_LINE_LOW
  jsr print_byte

  lda #':'
  jsr print_char
  lda #' '
  jsr print_char

  phx
  ldx #$08
  ldy #0

lineloop:
  lda (monitor::CURR_LINE_LOW),Y
  jsr print_byte
  lda #' '
  jsr print_char

  iny
  dex
  bne lineloop

  lda monitor::CURR_LINE_LOW
  clc
  adc #$08
  sta monitor::CURR_LINE_LOW
  lda #0
  adc monitor::CURR_LINE_HIGH
  sta monitor::CURR_LINE_HIGH
  jsr newline

  plx
  dex
  bne screenloop

  jsr rx_get_char
  jsr kbd_get_char
  bcc disploop

  cmp #$18
  beq up_arrow
  cmp #$1E
  beq page_up

  cmp #$19
  beq down_arrow
  cmp #$1F
  beq page_down

  jmp disploop

page_up:
  stz monitor::SCROLL_LENGTH_LOW
  lda #1
  sta monitor::SCROLL_LENGTH_HIGH
  jmp scroll_up
up_arrow:
  lda #$08
  sta monitor::SCROLL_LENGTH_LOW
  stz monitor::SCROLL_LENGTH_HIGH
scroll_up:
  clc
  lda monitor::CURR_ADDR_LOW
  adc monitor::SCROLL_LENGTH_LOW
  sta monitor::CURR_ADDR_LOW

  lda monitor::CURR_ADDR_HIGH
  adc monitor::SCROLL_LENGTH_HIGH
  sta monitor::CURR_ADDR_HIGH

  jmp done

page_down:
  stz monitor::SCROLL_LENGTH_LOW
  lda #1
  sta monitor::SCROLL_LENGTH_HIGH
  jmp scroll_down
down_arrow:
  lda #$08
  sta monitor::SCROLL_LENGTH_LOW
  stz monitor::SCROLL_LENGTH_HIGH
scroll_down:
  sec
  lda monitor::CURR_ADDR_LOW
  sbc monitor::SCROLL_LENGTH_LOW
  sta monitor::CURR_ADDR_LOW

  lda monitor::CURR_ADDR_HIGH
  sbc monitor::SCROLL_LENGTH_HIGH
  sta monitor::CURR_ADDR_HIGH

  jmp done

done:
  jmp disploop
.endproc
