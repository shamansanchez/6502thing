.include "addr.inc"
.include "macros.inc"
.autoimport

.export spi_byte
.proc spi_byte
  phx

  stz spi::SERIALIN
  sta spi::SERIALOUT
  ldx #8
  lda #spi::MOSI

loop:
  asl spi::SERIALOUT
  bcs set
  trb via2::PORTB
  jmp read

set:
  tsb via2::PORTB

read:
  inc via2::PORTB
  bit via2::PORTB
  clc
  bvc zero
  sec
zero:
  rol spi::SERIALIN
  dec via2::PORTB

  dex
  bne loop
  lda spi::SERIALIN
  clc

  plx
  rts
.endproc

.export spi_byte_sd
.proc spi_byte_sd
  phx

  stz spi::SDIN
  sta spi::SDOUT
  ldx #8
  lda #spi::MOSI

loop:
  asl spi::SDOUT
  bcs set
  trb via::PORTA
  jmp read

set:
  tsb via::PORTA

read:
  inc via::PORTA
  bit via::PORTA
  clc
  bvc zero
  sec
zero:
  rol spi::SDIN
  dec via::PORTA

  dex
  bne loop
  lda spi::SDIN
  clc

  plx
  rts
.endproc
