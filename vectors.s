.import _init, _irq, _nmi

.segment "VECTORS"
.addr _nmi    ;nmi
.addr _init   ;reset
.addr _irq    ;irq
