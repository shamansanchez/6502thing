PCBNEW-LibModule-V1  2023-03-29 04:18:18
# encoding utf-8
Units mm
$INDEX
DIP762W56P254L1905H457Q14N
$EndINDEX
$MODULE DIP762W56P254L1905H457Q14N
Po 0 0 0 15 6423adfa 00000000 ~~
Li DIP762W56P254L1905H457Q14N
Cd 14 PDIP
Kw Integrated Circuit
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "DIP762W56P254L1905H457Q14N"
DS -4.64 -9.965 4.64 -9.965 0.05 24
DS 4.64 -9.965 4.64 9.965 0.05 24
DS 4.64 9.965 -4.64 9.965 0.05 24
DS -4.64 9.965 -4.64 -9.965 0.05 24
DS -3.935 -9.715 3.935 -9.715 0.1 24
DS 3.935 -9.715 3.935 9.715 0.1 24
DS 3.935 9.715 -3.935 9.715 0.1 24
DS -3.935 9.715 -3.935 -9.715 0.1 24
DS -3.935 -8.445 -2.665 -9.715 0.1 24
DS -4.39 -9.715 3.935 -9.715 0.2 21
DS -3.935 9.715 3.935 9.715 0.2 21
$PAD
Po -3.81 -7.62
Sh "1" R 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 -5.08
Sh "2" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 -2.54
Sh "3" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 0
Sh "4" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 2.54
Sh "5" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 5.08
Sh "6" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -3.81 7.62
Sh "7" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 7.62
Sh "8" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 5.08
Sh "9" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 2.54
Sh "10" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 0
Sh "11" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 -2.54
Sh "12" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 -5.08
Sh "13" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.81 -7.62
Sh "14" C 1.16 1.16 0 0 900
Dr 0.76 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE DIP762W56P254L1905H457Q14N
$EndLIBRARY
