PCBNEW-LibModule-V1  2023-02-14 06:03:07
# encoding utf-8
Units mm
$INDEX
RCJ014
$EndINDEX
$MODULE RCJ014
Po 0 0 0 15 63eb241b 00000000 ~~
Li RCJ014
Cd RCJ-014-1
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 0.000 -5.625 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0.000 -5.625 1.27 1.27 0 0.254 N I 21 N "RCJ014"
DS -5.3 -13.2 5.3 -13.2 0.2 24
DS 5.3 -13.2 5.3 1.3 0.2 24
DS 5.3 1.3 -5.3 1.3 0.2 24
DS -5.3 1.3 -5.3 -13.2 0.2 24
DS -7.95 -14.2 7.95 -14.2 0.1 24
DS 7.95 -14.2 7.95 2.95 0.1 24
DS 7.95 2.95 -7.95 2.95 0.1 24
DS -7.95 2.95 -7.95 -14.2 0.1 24
DS -5.3 -2.45 -5.3 -13.2 0.1 21
DS -5.3 -13.2 5 -13.2 0.1 21
DS 5 -13.2 5.3 -13.2 0.1 21
DS 5.3 -13.2 5.3 -4.45 0.1 21
$PAD
Po 0.000 -0
Sh "2" C 2.550 2.550 0 0 900
Dr 1.7 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -5.000 -0
Sh "A1" C 3.900 3.900 0 0 900
Dr 2.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 -4.5
Sh "B1" C 3.900 3.900 0 0 900
Dr 2.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.000 -0
Sh "C1" C 3.900 3.900 0 0 900
Dr 2.6 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE RCJ014
$EndLIBRARY
