.include "addr.inc"

.code
lcd_init:

  lda #%11101111
  sta via::DDRA

  jsr init_4bit

  lda #%00101000
  jsr lcd_instruction

  lda #%00001111
  jsr lcd_instruction

  lda #%00000110
  jsr lcd_instruction

  lda #%00000001
  jsr lcd_instruction

lcd_print:
  phy
  pha
  ldy #0
print_loop:
  lda (PRINT_LOW), Y
  beq print_done
  jsr print_char
  iny
  jmp print_loop
print_done:
  pla
  ply
  rts

init_4bit:
  pha
  lda #%00000010
  sta via::PORTA

  ora #lcd::ENABLE
  sta via::PORTA
  eor #lcd::ENABLE
  sta via::PORTA

  pla
  rts

lcd_instruction:
  jsr lcd_wait
  pha

  lsr
  lsr
  lsr
  lsr

  ; send upper 4 bits
  sta via::PORTA
  ora #lcd::ENABLE
  sta via::PORTA
  eor #lcd::ENABLE
  sta via::PORTA

  pla
  and #%00001111

  ; send lower 4 bits
  sta via::PORTA
  ora #lcd::ENABLE
  sta via::PORTA
  eor #lcd::ENABLE
  sta via::PORTA

  rts

print_char:
  jsr lcd_wait
  pha

  lsr
  lsr
  lsr
  lsr

  ; send upper 4 bits
  ora #lcd::RS
  sta via::PORTA
  ora #lcd::ENABLE
  sta via::PORTA
  eor #lcd::ENABLE
  sta via::PORTA

  pla
  and #%00001111

  ; send lower 4 bits
  ora #lcd::RS
  sta via::PORTA
  ora #lcd::ENABLE
  sta via::PORTA
  eor #lcd::ENABLE
  sta via::PORTA

  rts

lcd_wait:
  pha

  lda #%11100000  ; Data bits on port A are input
  sta via::DDRA
lcd_busy:
  lda #lcd::RW
  sta via::PORTA

  lda #(lcd::RW | lcd::ENABLE)
  sta via::PORTA
  lda via::PORTA
  pha
  lda #lcd::RW
  sta via::PORTA
  lda #(lcd::RW | lcd::ENABLE)
  sta via::PORTA
  lda via::PORTA

  pla


  and #%00001000
  bne lcd_busy

  lda #lcd::RW
  sta via::PORTA

  lda #%11101111  ; Port A is output
  sta via::DDRA
  pla
  rts
