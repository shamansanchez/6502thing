.include "addr.inc"
.include "macros.inc"
.autoimport

.export newline
.export print_str
.export print_char
.export clear_screen
.export scroll_down



.code

.proc print_str
  pha

print_loop:  ; string_addr --
  lda (0,X)
  beq print_done

  jsr print_char

  inc 0,X
  bne print_loop

  inc 1,X
  jmp print_loop

print_done:
  pop
  pla
  rts
.endproc

; ==============================
.proc print_char
  pha
  phy

  cmp #$0a
  beq linefeed

  cmp #$0d
  beq done

  cmp #$08
  beq backspace

  jmp print

linefeed:
  jsr newline
  jmp done

backspace:
  dec video::CURSOR_LOW
  lda #$20
  sta (video::CURSOR_LOW)
  jmp done

print:
  clc
  cmp #$80
  bcs uppercase

  cmp #$60
  bcc uppercase
  beq uppercase

  sbc #$60

uppercase:
  sta (video::CURSOR_LOW)

  inc video::CURSOR_LOW
  bne done

  inc video::CURSOR_HIGH
  bbr0 video::CURSOR_HIGH, scroll
  jmp done

scroll:
  jsr scroll_down

done:
  ply
  pla
  rts
.endproc


; ==============================
.export print_byte
.proc print_byte  ;  str_bytes --
  pha
  phx

  jsr str_byte

  jsr print_char
  txa
  jsr print_char

  plx
  pla
  rts
.endproc


; ==============================
.proc newline
  pha
  phx
  phy

  lda #$20
  sta (video::CURSOR_LOW)

  lda video::CURSOR_LOW

  lsr  ; divide by 32
  lsr
  lsr
  lsr
  lsr

  clc
  adc #01  ; then add one

  asl   ; then multiply by 32 to get to the next multiple
  asl
  asl
  asl
  asl

  sta video::CURSOR_LOW
  bcc done

  inc video::CURSOR_HIGH
  bbs0 video::CURSOR_HIGH,done
  jsr scroll_down

done:
  ply
  plx
  pla
  rts
.endproc


; ==============================
.proc clear_screen
  pha
  phy

start_clear:
  ldy #0
  lda #$20

loop_clear:
  sta video::BASE, Y
  sta video::BASE+$100, Y
  iny
  beq done_clear
  jmp loop_clear

done_clear:
  lda #<video::BASE
  sta video::CURSOR_LOW
  lda #>video::BASE
  sta video::CURSOR_HIGH

  ply
  pla
  rts
.endproc


; ==================================
.proc scroll_down
  pha
  phx

  lda #<video::BASE + $20
  sta video::CURSOR_LOW
  lda #<video::BASE
  sta video::CURSOR2_LOW

  lda #>video::BASE
  sta video::CURSOR_HIGH
  sta video::CURSOR2_HIGH

loop:
  lda (video::CURSOR_LOW)
  sta (video::CURSOR2_LOW)

  inc video::CURSOR_LOW
  beq cursor1_flip

cursor2:
  inc video::CURSOR2_LOW
  beq cursor2_flip

  jmp loop


cursor1_flip:
  inc video::CURSOR_HIGH
  bbr0 video::CURSOR_HIGH, done
  jmp cursor2

cursor2_flip:
  inc video::CURSOR2_HIGH
  jmp loop

done:
  lda #<video::BASE + $e0
  sta video::CURSOR_LOW
  lda #>video::BASE + 1
  sta video::CURSOR_HIGH

  ldx #$20
  lda #$20

clear:
  sta (video::CURSOR_LOW)
  inc video::CURSOR_LOW
  dex
  bne clear

  lda #<video::BASE + $e0
  sta video::CURSOR_LOW
  lda #>video::BASE + 1
  sta video::CURSOR_HIGH

  plx
  pla
  rts
.endproc
