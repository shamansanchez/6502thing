import yaml

with open("./portfolio.yaff") as f:
    y = yaml.safe_load(f)

    for k in y:
        s = y[k]
        print(f"; {hex(k)}: {chr(k)}")
        for l in s.split(" "):
            print(".byte %" + l)
